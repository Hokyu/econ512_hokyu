function [fval] = Lindiv1(beta,f,g)
fval=1;
for i=1:20
    fval=fval*(exp(beta*f(i))/(1+exp(beta*f(i))))^(g(i))*(1/(1+exp(beta*f(i))))^(1-g(i));
end

