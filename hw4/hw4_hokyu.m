%hw4 by Hokyu Song
clear;clc;
global gamma beta0 u0 sigmab sigmabu sigmau
load('hw4data.mat');
X=data.X; Y=data.Y; Z=data.Z;
%1.
[x,w] = qnwnorm(20,0.1,1);
L=1;
for i=1:100
    intLi=0;
    for j=1:20
intLi=Lindiv1(x(j),X(:,i),Y(:,i))*w(j)
    end
    L=L*intLi;
end
L%value of the likelihood function. Note that this value is too small
% this is because you calculate it incorrectly. take a look at answer key