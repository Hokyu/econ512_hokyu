function [Pstar]=qfoc(P)
V=[-1;-1;-1];
Pstar=1-P.*(1-(exp(V-P))./(1+sum(exp(V-P))));%foc derived from the model
end