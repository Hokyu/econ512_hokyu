%HW2 by Hokyu Song
clear; clc;
addpath('C:/users/hokyu/Desktop/empirical_methods/econ_512_2017/CEtools/')
% use relative path like ../econ_512_2017
%% Q1
V=[-1;-1;-1];
P=[1;1;1];
Q1qA=quant(P);Q1qB=quant(P);Q1qC=quant(P);
 Q1q0=1/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
 
 %% Q2
 tol=sqrt(eps)%use the default convergence criteria in broyden code
%a.
tic
P=[1;1;1];
Pstar=broyden(@(x) qfoc(x),P);
% extract also the number of iterations it took for the algorithm to
% converge in a separate variable.
toc
%b.
tic
P=[0;0;0];
Pstar=broyden(@(x) qfoc(x),P);
toc
%c.
tic
P=[0;1;2];
Pstar=broyden(@(x) qfoc(x),P);
toc
%d.
tic
P=[3;2;1];
Pstar=broyden(@(x) qfoc(x),P);
toc

%% Q4

tic
P4=[1;1;1];
newP4=P4+1;
j=1;
while norm(P4-newP4)>tol
    P4=newP4;   
    q = exp(V-P4)./(1+sum(exp(V-P4)));
    newP4 = ones(3,1)./(1-q);
    j = j+1;
    if j >= 10000
        break;
    end
end
toc
%b.
tic
P4=[0;0;0];
newP4=P4+1;
j=1;
while norm(P4-newP4)>tol
    P4=newP4;   
    q = exp(V-P4)./(1+sum(exp(V-P4)));
    newP4 = ones(3,1)./(1-q);
    j = j+1;
    if j >= 10000
        break;
    end
end
toc
%c.
tic
P4=[0;1;2];
newP4=P4+1;
j=1;
while norm(P4-newP4)>tol
    P4=newP4;   
    q = exp(V-P4)./(1+sum(exp(V-P4)));
    newP4 = ones(3,1)./(1-q);
    j = j+1;
    if j >= 10000
        break;
    end
end
toc
%d.
tic
P4=[3;2;1];
newP4=P4+1;
j=1;
while norm(P4-newP4)>tol
    P4=newP4;   
    q = exp(V-P4)./(1+sum(exp(V-P4)));
    newP4 = ones(3,1)./(1-q);
    j = j+1;
    if j >= 10000
        break;
    end
end
toc
%By observing the result myself, q4 method is faster than the Broyden's method.

% question #5?