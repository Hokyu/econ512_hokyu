function [q]=quant(P)
 V=[-1,-1,-1];%fixed parameter value
    q=exp(V(1)-P(1)) / (1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
end