%HW1 by Hokyu Song
diary
%% Q1
clear;clc;
X1=[1 1.5 3 4 5 7 9 10];
Y1=-2+0.5*X1; Y2=-2+0.5*X1.^2;
plot(X1,Y1,X1,Y2); legend('Y1','Y2'); title('Q1 Answer');

%% Q2
X2=linspace(-10,20,200);
Q2ans=X2*ones(200,1)

%% Q3
A=[2 4 6; 1 7 5; 3 12 4]; B=[-2 3 10]';
C=A'*B 
D=(A'*A)^-1*B 
E=ones(1,3)*(A.*B)*ones(3,1) 
F=A(1,:) 
x=A\B

%% Q4
% learn to use kron(), this way is too burdensome
B4=[A zeros(3) zeros(3) zeros(3) zeros(3);
    zeros(3) A zeros(3) zeros(3) zeros(3);
    zeros(3) zeros(3) A zeros(3) zeros(3);
    zeros(3) zeros(3) zeros(3) A zeros(3);
    zeros(3) zeros(3) zeros(3) zeros(3) A];

%% Q5
A5=random('norm',10,5,[5,3]);
% there is faster way to do that
for i=1:5;
    for j=1:3;
        if A5(i,j)<10, A5(i,j)=0;
        else A5(i,j)=1;
        end
    end
end

%% Q6

load('datahw1.csv');
X=[ones(length(datahw1),1) datahw1(:,3:4) datahw1(:,6)];
Y=datahw1(:,5);
beta=(X'*X)^-1*X'*Y %OLS estimator
fitlm(X(:,2:4),Y)
diary