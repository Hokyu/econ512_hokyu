function [f,g] = fun2(beta,X,y)

    f = -sum(-exp(X*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)])...
        +y.*(X*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)])-log(factorial(y)));
        g =  0;
        for i = 1:length(y)
         g = g  - (X(i,:)'*(-exp(X(i,:)*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)]))+X(i,:)'*y(i));
        end
end