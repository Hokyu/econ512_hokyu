function [x, fjac]=func(theta,y)
x=psi(0,theta)-log(theta)+log(y);
fjac=psi(1,theta)-1/theta;
end