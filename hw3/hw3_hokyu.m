%HW3 by Hokyu Song
clear;clc;
addpath('C:/Users/Hokyu/Desktop/empirical_methods/econ_512_2017/CEtools/')
% please use relative path, not absolute path
%% Part A
% a&b are solved in pdf
%% c
% Suppose there is a data X=[x_1,x_2,...,x_n]'. Then Y1=ones(1,n)*X/n,
% Y2=exp(ones(1,n)*log(X)/n). Then thetaHat1=newton(@(theta) func(theta,Y1/Y2),1) 
% and thetaHat2=thetaHat1/Y1.
%% d
%Let Y=Y1/Y2
Y=1.1:0.01:3;
thetaHat1=zeros(1,length(Y));
for i=1:length(Y);
    thetaHat1(i)=newton(@(theta) func(theta,Y(i)),1);
    % you were not supposed to use generic optimizers, only hand written
    % Newton's method
end
figure(1)
hold on
plot(Y,thetaHat1)
xlabel('Y1/Y2'); ylabel('theta1')
hold off

%% Part B
load('HW3.mat');
%% 1
% fminunc without a derivative
func1 = @(beta)-sum(-exp(X*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)])...
    +y.*(X*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)])-log(factorial(y)));

[beta1,fval,exitflag,output1] = fminunc(func1,[0;0;0;0;0;0])

% fminunc with a derivative
option = optimoptions('fminunc','SpecifyObjectiveGradient',true);
[beta2,fval,exitflag,output2] = fminunc(@(beta) func2(beta,X,y),[0;0;0;0;0;0],option)

% Nelder Mead
option2=optimset('TolFun',1e-12,'MaxFunEvals',1e10);
[beta3,fval,exitflag,output3] = fminsearch(func1,[0;0;0;0;0;0],option2)

% BHHH

temp_beta4 = [0;0;0;0;0;0];
iter4 = 1;
while 1
      diff =  0;
        for i = 1:length(y)
         diff = diff  + (X(i,:)'*(-exp(X(i,:)*[temp_beta4(1);temp_beta4(2);temp_beta4(3);temp_beta4(4)...
             ;temp_beta4(5);temp_beta4(6)]))+X(i,:)'*y(i));
        end
    S = 0;
    for i = 1:length(y)
        s = (X(i,:)'*(-exp(X(i,:)*[temp_beta4(1);temp_beta4(2);temp_beta4(3);temp_beta4(4);temp_beta4(5);temp_beta4(6)]))...
    +X(i,:)'*y(i));
        S = S - s*s';
    end
    beta_temp = temp_beta4 - S\diff;
    old_beta4 = temp_beta4;
    temp_beta4 = beta_temp;
    if norm(temp_beta4 - old_beta4)<1e-12
        break; 
    end
    iter4 = iter4 + 1;
end
beta4 = temp_beta4 
iter4

%% 2
H = 0;
for i = 1:length(y)
    H = H +  X(i,:)'*(X(i,:)*(-exp(X(i,:)*[beta4(1);beta4(2);beta4(3);beta4(4)...
             ;beta4(5);beta4(6)])));
end

eH = eig(H) %eigenvalues of Hessian
eS = eig(S) %eigenvalues of estimates

%% 3

new_beta5 = [0;0;0;0;0;0];
iter5 = 1;
while 1
    J = zeros(length(y),length(beta1));
    for i = 1:length(y)
        for j = 1:length(beta1)
            J(i,j) = -X(i,j)*(exp(X(i,:)*[new_beta5(1);new_beta5(2);new_beta5(3);new_beta5(4)...
             ;new_beta5(5);new_beta5(6)]));
        end
    end
    f = zeros(length(y),1);
    for i = 1:length(y)
       f(i) = (y(i)-exp(X(i,:)*[new_beta5(1);new_beta5(2);new_beta5(3);new_beta5(4)...
             ;new_beta5(5);new_beta5(6)])); 
    end
    beta_temp = new_beta5 - (J'*J)\J'*f;
    old_beta5 = new_beta5;
    new_beta5 = beta_temp;
    if norm(new_beta5 - old_beta5)<1e-12
        break; 
    end
    iter5 = iter5 + 1;
end

beta5 = new_beta5 
iter5
%% 4

std_BHHH = diag(sqrt(inv(-H)./length(y))) %Standard deviation for BHHH

D = zeros(6,6);
V = 0;
for i = 1:length(y)
    D = D + (X(i,:)'.*exp(X(i,:)*beta5)^2.*X(i,:));
    V = V + (y(i) - exp(X(i,:)*beta5))^2.*(X(i,:)'.*exp(X(i,:)*beta5)^2.*X(i,:));
end
V = 4/length(y).*V;
D = 1/length(y).*D;
psi = D\V/D;
std_NLLS = diag(sqrt(inv(psi)./length(y))) %Standard Deviation for NLLS

